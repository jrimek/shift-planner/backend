class FiltersController < ApplicationController
  def people
    week = params['week'].to_i
    dept = params['dept_id'].to_i
    search_criteria = Person.filter_criteria(dept, week)

    render json: search_criteria
  end
end
