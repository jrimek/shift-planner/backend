class PersonShiftsController < ApplicationController
  before_action :set_person_shift, only: [:show, :update, :destroy]

  # GET /person_shifts
  def index
    #unused and doesnt seem to work
    person_id = params[:person_id].to_i
    week = params[:week].to_i

    render json: Shift.get_shifts_for_person(person_id, week)
  end
end