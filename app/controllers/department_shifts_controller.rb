class DepartmentShiftsController < ApplicationController
  before_action :set_department_shift, only: [:show, :update, :destroy]

  # GET /department_shifts
  def index 
    #unused
    dept_id = params[:dept_id].to_i
    week = params[:week].to_i

    render json: Shift.get_shifts_for_dept(dept_id, week)
  end
end
