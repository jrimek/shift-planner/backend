class Shift < ApplicationRecord
  belongs_to :person
  belongs_to :shift_type

  def self.get_shifts_for_dept(dept_id, week)
      #list = people_in_dept_as_list(id)
      
      Shift.find_by_sql ["
SELECT array_to_json(array_agg(row_to_json(persons)))
FROM (SELECT p.id, p.name, json_agg(x) as shifts
      FROM people p
      CROSS JOIN LATERAL (SELECT d.date_of_shift, sh.id as shift_id, sh.name, sh.shift_type_id
                          FROM generate_series(?::date, ?, interval '1 day') AS d(date_of_shift)
                          LEFT JOIN LATERAL (SELECT shifts.id, shifts.person_id, shifts.date_of_shift, shifts.shift_type_id, shift_types.name
                                             FROM shifts
                                             JOIN shift_types
                                             ON shift_types.id = shifts.shift_type_id
                                             ) as sh
                          ON sh.date_of_shift = d.date_of_shift AND sh.person_id = p.id
                          ) as x

      WHERE p.id IN (SELECT people.id
                     FROM people
                     WHERE people.department_id = ?)
      GROUP BY p.id)
AS persons

;


", start_of_week(week), end_of_week(week), dept_id]
 #       ELECT p.id, p.name, s.date_of_shift
 #       FROM people as p
 #       LEFT JOIN LATERAL (
 #               SELECT sh.id, sh.date_of_shift, sh.person_id
 #               FROM shifts as sh
 #       ) as s
 #       ON p.id = s.person_id
 #       WHERE p.id = 2 AND s.date_of_shift BETWEEN '2016-03-21' AND '2016-03-25'
 #       UNION ALL
 #       SELECT null, null, '2016-03-21'
  #      WHERE NOT EXISTS (
  #              select 1
   ##             FROM people as p
    #            LEFT JOIN LATERAL (
     #           SELECT sh.id, sh.date_of_shift, sh.person_id
    #            FROM shifts as sh
    ##            ) as s
     ##           ON p.id = s.person_id
    ##            WHERE p.id = 88000 AND
     #   s.date_of_shift BETWEEN  '2016-03-21' AND '2016-03-25')


      #AR solution takes 10 times more time: 65ms
      #query takes 6ms
#    array = []
#    Struct.new('Row', :person_id, :shifts)
#
#    Department.find(id).person.each do |person|
#      array << Struct::Row.new(
#        person.id,
#        get_shifts_for_person(person.id, week))
#    end
#    array
  end

  def self.get_shifts_for_person(id, week)
    #unused and doesnt seem to work
    Shift.find_by_sql ["
                SELECT p.id,
                       p.name,
                       json_agg(s) as shifts
                  FROM people as p
    CROSS JOIN LATERAL (SELECT d.date_of_shift, sh.id as shift_id, sh.shift_type_id, sh.name
                          FROM generate_series(?::date, ?, interval '1 day') AS d(date_of_shift)
             LEFT JOIN LATERAL (SELECT shifts.id, 
                                       shifts.person_id, 
                                       shifts.date_of_shift,  
                                       shifts.shift_type_id, 
                                       shift_types.name
                                  FROM shifts
                                  JOIN shift_types
                                    ON shift_types.id = shifts.shift_type_id
                               ) as sh
                            ON d.date_of_shift = sh.date_of_shift AND sh.person_id = p.id) as s
                 WHERE p.id = ?
                 GROUP BY p.id
      ", start_of_week(week), end_of_week(week), id]

  #  #AR takes 3ms and SQL takes 2ms. The time is so high because of the ?
  # inside the query, when I put the variables directly inside the string
  # the query only took 0,4ms
  #  array = []
  #  Struct.new('Shift', :shift_id, :date_of_shift, :shift_type_id)
  #  (start_of_week(week)..end_of_week(week)).each do |day|
  #    shift = Shift.where(person_id: id, date_of_shift: day)[0]
  #    array << Struct::Shift.new(
  #      shift.id,
  #      shift.date_of_shift,
  #      shift.shift_type_id)
  #  end
  #  array
  end

  def self.people_in_dept_as_list(id)
    person_list = '('
    Department.find(id).person.each do |p|
      person_list << p.id.to_s + ','
    end
    person_list[person_list.length - 1] = ''
    person_list + ')'
  end

  # I probably schould move this to an seperate class or smth
  def self.start_of_week(week = 0)
    week.weeks.from_now.beginning_of_week.to_date
  end

  def self.end_of_week(week = 0, length_of_week = 4) # equals a mo - fr week
    start_of_week(week) + length_of_week
  end
end
