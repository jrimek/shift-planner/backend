class Person < ApplicationRecord
  has_many :shift
  belongs_to :department
  has_and_belongs_to_many :language
  has_and_belongs_to_many :role
  has_and_belongs_to_many :skill

  validates :name, presence: true
  validates :department_id, presence: true
  # this is already covered by the fk_constrain,
  # but I wanted to have it in AR. to get better error messages

  def self.detailed_info_for_people_in_dept(dept_id, week)
    fast_query = "SELECT array_to_json(array_agg(row_to_json(persons))) as people_in_dept FROM (SELECT p.id, p.name, json_agg(DISTINCT(x)) as shifts,json_agg(DISTINCT(ps)) as skills, json_agg(DISTINCT(pr)) as roles, json_agg(DISTINCT(lp)) as languages FROM people p CROSS JOIN LATERAL (SELECT d.date_of_shift, sh.id, sh.name, sh.shift_type_id FROM generate_series(?::date, ?, interval '1 day') AS d(date_of_shift) LEFT JOIN LATERAL (SELECT shifts.id, shifts.person_id, shifts.date_of_shift, shifts.shift_type_id, shift_types.name FROM shifts JOIN shift_types ON shift_types.id = shifts.shift_type_id ) as sh ON sh.date_of_shift = d.date_of_shift AND sh.person_id = p.id ) as x LEFT JOIN LATERAL   (SELECT sk.id, s.person_id, sk.name FROM people_skills as s LEFT JOIN skills as sk ON sk.id = s.skill_id ) as ps ON ps.person_id = p.id LEFT JOIN LATERAL   (SELECT r.name, r.id, pero.person_id FROM roles as r LEFT JOIN people_roles as pero ON pero.role_id = r.id ) as pr ON pr.person_id = p.id LEFT JOIN LATERAL   (SELECT l.name, l.id, lape.person_id FROM languages as l LEFT JOIN languages_people as lape ON lape.language_id = l.id ) as lp ON lp.person_id = p.id WHERE p.department_id = ? GROUP BY p.id) AS persons"
    slow_query = "
                      SELECT array_to_json(array_agg(row_to_json(persons))) as people_in_dept
                FROM (SELECT p.id, 
                             p.name, 
                             json_agg(DISTINCT(pr)) as roles, 
                             json_agg(DISTINCT(ps)) as skills, 
                             json_agg(DISTINCT(lp)) as languages,
                             json_agg(DISTINCT(x)) as shifts

                             FROM people p
               CROSS JOIN LATERAL (SELECT d.date_of_shift, 
                                          sh.id, 
                                          sh.name, 
                                          sh.shift_type_id
                                     FROM generate_series('2016-03-07'::date, '2016-03-11', interval '1 day') AS d(date_of_shift)
                        LEFT JOIN LATERAL (SELECT shifts.id, shifts.person_id, shifts.date_of_shift, shifts.shift_type_id, shift_types.name
                                             FROM shifts
                                             JOIN shift_types
                                               ON shift_types.id = shifts.shift_type_id
                                          ) as sh
                                       ON sh.date_of_shift = d.date_of_shift AND 
                                          sh.person_id = p.id
                             ) as x

           LEFT JOIN LATERAL (     SELECT sk.id, 
                                          s.person_id, 
                                          sk.name
                                     FROM people_skills as s
                                LEFT JOIN skills as sk
                                    ON sk.id = s.skill_id
                              ) as ps
                          ON ps.person_id = p.id

           LEFT JOIN LATERAL (     SELECT r.name, 
                                          r.id, 
                                          pero.person_id
                                     FROM roles as r
                                LEFT JOIN people_roles as pero
                                       ON pero.role_id = r.id
                             ) as pr
                          ON pr.person_id = p.id

           LEFT JOIN LATERAL (     SELECT l.name, 
                                          l.id, 
                                          lape.person_id
                                     FROM languages as l
                                LEFT JOIN languages_people as lape
                                       ON lape.language_id = l.id
                             ) as lp
                          ON lp.person_id = p.id
                       WHERE p.department_id = 43
                       GROUP BY p.id)
             AS persons"
    
    find_by_sql [fast_query, start_of_week(week), end_of_week(week), dept_id]
    #Person.find_by_sql [slow_query, start_of_week(week), end_of_week(week), dept_id]
  end

  def self.filter_criteria(dept_id, week)
    
    slow_query = "
SELECT row_to_json(result) as filter_criterias
FROM (SELECT (SELECT json_agg(roles) AS roles
        FROM (SELECT r.name, r.id
              FROM roles AS r
              WHERE r.id IN (SELECT pr.role_id
                              FROM people_roles AS pr
                              JOIN (SELECT p.id
                                      FROM people AS p
                                     WHERE p.department_id = ?
                                   ) AS  p
                                ON p.id = pr.person_id))
              AS roles),
       (SELECT json_agg(skills) AS skills
        FROM (SELECT s.id, s.name
              FROM skills AS s
             WHERE s.id IN (SELECT ps.skill_id
                              FROM people_skills AS ps
                              JOIN (SELECT p.id
                                      FROM people AS p
                                     WHERE p.department_id = ?
                                   ) AS  p
                                ON p.id = ps.person_id
                          GROUP BY ps.skill_id
                          ORDER BY ps.skill_id)
                      )
        AS skills),
       (SELECT json_agg(languages) as languages
        FROM (SELECT l.id, l.name
              FROM languages AS l
             WHERE l.id IN (SELECT lp.language_id
                              FROM languages_people AS lp
                              JOIN (SELECT p.id
                                      FROM people AS p
                                     WHERE p.department_id = ?
                                   ) AS  p
                                ON p.id = lp.person_id
                          GROUP BY lp.language_id
                          ORDER BY lp.language_id)
                      )
        AS languages),
       (SELECT json_agg(shifts) as shifts
        FROM (SELECT st.id, st.name
              FROM shift_types as st
             WHERE st.id IN (SELECT sh.shift_type_id
                               FROM shifts as sh
                              WHERE person_id IN (SELECT p.id
                                                    FROM people as p
                                                   WHERE p.department_id = ?)
                                    AND
                                    sh.date_of_shift BETWEEN ? AND ?
                           GROUP BY sh.shift_type_id)
                     ) as shifts)) as result
;"
    fast_query = "SELECT row_to_json(result) as filter_criterias FROM (SELECT (SELECT json_agg(roles) AS roles FROM (SELECT r.name, r.id FROM roles AS r WHERE r.id IN (SELECT pr.role_id FROM people_roles AS pr JOIN (SELECT p.id FROM people AS p WHERE p.department_id = ? ) AS  p ON p.id = pr.person_id)) AS roles), (SELECT json_agg(skills) AS skills FROM (SELECT s.id, s.name FROM skills AS s WHERE s.id IN (SELECT ps.skill_id FROM people_skills AS ps JOIN (SELECT p.id FROM people AS p WHERE p.department_id = ? ) AS  p ON p.id = ps.person_id GROUP BY ps.skill_id ORDER BY ps.skill_id) ) AS skills), (SELECT json_agg(languages) as languages FROM (SELECT l.id, l.name FROM languages AS l WHERE l.id IN (SELECT lp.language_id FROM languages_people AS lp JOIN (SELECT p.id FROM people AS p WHERE p.department_id = ? ) AS  p ON p.id = lp.person_id GROUP BY lp.language_id ORDER BY lp.language_id) ) AS languages), (SELECT json_agg(shifts) as shifts FROM (SELECT st.id, st.name FROM shift_types as st WHERE st.id IN (SELECT sh.shift_type_id FROM shifts as sh WHERE person_id IN (SELECT p.id FROM people as p WHERE p.department_id = ?) AND sh.date_of_shift BETWEEN ? AND ? GROUP BY sh.shift_type_id) ) as shifts)) as result ;"   
    result = find_by_sql [fast_query, dept_id, dept_id, dept_id, dept_id, start_of_week(week), end_of_week(week)]
    result = result[0]['filter_criterias']  

  end

  def self.detailed_info(id, week)
    find_by_sql ["
             SELECT p.id,
                    p.name,
                    json_agg(DISTINCT(ps)) as skills,
                    json_agg(DISTINCT(pr)) as roles,
                    json_agg(DISTINCT(lp)) as languages,
                    json_agg(DISTINCT(s)) as shifts
           
               FROM people as p

  LEFT JOIN LATERAL   (SELECT skill_id, s.person_id, sk.name
                         FROM people_skills as s
                    LEFT JOIN skills as sk
                           ON sk.id = s.skill_id
                   ) as ps
                 ON ps.person_id = p.id

  LEFT JOIN LATERAL   (SELECT r.name, r.id, pero.person_id
                         FROM roles as r
                    LEFT JOIN people_roles as pero
                           ON pero.role_id = r.id
                    ) as pr
                 ON pr.person_id = p.id
                 
  LEFT JOIN LATERAL   (SELECT l.name, l.id, lape.person_id
                         FROM languages as l
                    LEFT JOIN languages_people as lape
                           ON lape.person_id = l.id
                    ) as lp
                 ON lp.person_id = p.id

 CROSS JOIN LATERAL   (SELECT d.date_of_shift, 
                              sh.id as shift_id, 
                              sh.shift_type_id, 
                              sh.name
                         FROM generate_series(?::date, ?, interval '1 day') AS d(date_of_shift)
            LEFT JOIN LATERAL (SELECT shifts.id, 
                                      shifts.person_id, 
                                      shifts.date_of_shift,  
                                      shifts.shift_type_id, 
                                      shift_types.name
                                 FROM shifts
                                 JOIN shift_types
                                   ON shift_types.id = shifts.shift_type_id
                              ) as sh
                           ON d.date_of_shift = sh.date_of_shift AND sh.person_id = p.id) as s

              WHERE p.id = ?
           GROUP BY p.id
    ", start_of_week(week), end_of_week(week), id]
  end
  
  def self.start_of_week(week = 0)
    week.weeks.from_now.beginning_of_week.to_date
  end

  def self.end_of_week(week = 0, length_of_week = 4) # equals a mo - fr week
    start_of_week(week) + length_of_week
  end
end
