require 'rails_helper'

RSpec.describe FiltersController, type: :controller do

  describe "GET #people" do
    it "returns http success" do
      get :people
      expect(response).to have_http_status(:success)
    end
  end

end
