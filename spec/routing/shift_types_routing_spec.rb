require "rails_helper"

RSpec.describe ShiftTypesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/shift_types").to route_to("shift_types#index")
    end

    it "routes to #new" do
      expect(:get => "/shift_types/new").to route_to("shift_types#new")
    end

    it "routes to #show" do
      expect(:get => "/shift_types/1").to route_to("shift_types#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/shift_types/1/edit").to route_to("shift_types#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/shift_types").to route_to("shift_types#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/shift_types/1").to route_to("shift_types#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/shift_types/1").to route_to("shift_types#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/shift_types/1").to route_to("shift_types#destroy", :id => "1")
    end

  end
end
