require "rails_helper"

RSpec.describe DepartmentShiftsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/department_shifts").to route_to("department_shifts#index")
    end

    it "routes to #new" do
      expect(:get => "/department_shifts/new").to route_to("department_shifts#new")
    end

    it "routes to #show" do
      expect(:get => "/department_shifts/1").to route_to("department_shifts#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/department_shifts/1/edit").to route_to("department_shifts#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/department_shifts").to route_to("department_shifts#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/department_shifts/1").to route_to("department_shifts#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/department_shifts/1").to route_to("department_shifts#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/department_shifts/1").to route_to("department_shifts#destroy", :id => "1")
    end

  end
end
