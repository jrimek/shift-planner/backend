require "rails_helper"

RSpec.describe PersonShiftsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/person_shifts").to route_to("person_shifts#index")
    end

    it "routes to #new" do
      expect(:get => "/person_shifts/new").to route_to("person_shifts#new")
    end

    it "routes to #show" do
      expect(:get => "/person_shifts/1").to route_to("person_shifts#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/person_shifts/1/edit").to route_to("person_shifts#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/person_shifts").to route_to("person_shifts#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/person_shifts/1").to route_to("person_shifts#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/person_shifts/1").to route_to("person_shifts#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/person_shifts/1").to route_to("person_shifts#destroy", :id => "1")
    end

  end
end
