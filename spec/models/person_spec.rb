require 'rails_helper'

RSpec.describe Person, type: :model do
  FactoryGirl.define do
    factory :person do
    end

    factory :department do
    end
  end

  it 'schould have a department_id and a name' do
    dept = FactoryGirl.create(:department)
    puts dept.id
    person = FactoryGirl.build(:person, department_id: dept.id, name: 'Jonny')
    person_without_name = FactoryGirl.build(:person, department_id: dept.id)
    person_without_dept = FactoryGirl.build(:person, name: 'Jonny')
    expect(person.valid?).to be_truthy
    expect(person_without_dept.valid?).to be_falsy
    expect(person_without_name.valid?).to be_falsy
  end

  it 'schould test the sql query of detailed_info' do
    # honestly I'm not sure how to test the result, because parts of it are json
    # and I can't really access the data to test it
  end

  it 'schould calc start and end of weeks correctly' do
    date_at_monday = Date.today.beginning_of_week
    date_at_friday = Date.today.beginning_of_week + 4

    expect(Person.start_of_week).to eq(date_at_monday)
    expect(Person.end_of_week(0, 4)).to eq(date_at_friday)
  end
end
