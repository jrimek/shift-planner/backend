# README

### update and summary

for now I will stop working on this project and start a new one. With this new version I learned way more about SQL, AngularJS and generally how to structure the code in rails. My commit messages also improved. In the v1 I had way to many logic in the views and empty models -.- I didn't get to use as much TDD as I wanted to and I will definately have to improve on that.
Overall I'm pretty happy with what I learned even though I kinda lost motivation in the end.

### **IMPORTANT!** 

this branch is a **complete** rework and not completely finished, for a more complete version checkout 
[v1](https://github.com/jimbo-on-ruby/shift-planner/tree/v1), but be aware that it was a prototype and the code is pretty crappy.
I moved the angular part to a seperate repository, you can find it [here](https://github.com/jimbo-on-ruby/shift-planner-frontend).

### why am I doing this?

I chose this project for my IHK-exam. You can see the documentation [here](https://github.com/jimbo-on-ruby/ihk-abschlusspruefung).

### what will be better in this version?

- rails 5 --api + AngularJS
- close interaction with psql (custom SQL queries)
- TDD (RSpec)
- thinner controllers, logic will be moved to the model
- better names for the tables, the [structure](https://docs.google.com/drawings/d/1F9luZAEVBCcHlaLR3fr-LHdsML-qRErXUUibstjluJY) stays the same
- better commit messages (rebase + [guideline](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html))
