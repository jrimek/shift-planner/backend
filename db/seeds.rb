# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# clear table:
# Language.delete_all;Role.delete_all;Skill.delete_all;Shift.delete_all;ShiftType.delete_all;Person.delete_all;Department.delete_all

10.times do |i|
  Role.create(name: "role#{i}")
end

10.times do |i|
  Skill.create(name: "skill#{i}")
end

10.times do |i|
  Language.create(name: "language#{i}")
end

10.times do |i|
  ShiftType.create(name: "shift_type#{i}")
end

10.times do |i|
  dept = Department.create(name: "department#{i}")

  50.times do |j|
    person = Person.create(name: "person#{j}", department_id: dept.id)

    rand(3).times do |_l|
      person.role << Role.find(Role.first.id + rand(10))
    end
    rand(3).times do |_l|
      person.skill << Skill.find(Skill.first.id + rand(10))
    end
    rand(3).times do |_l|
      person.language << Language.find(Language.first.id + rand(10))
    end
  end
end

weeks = -50..100
people = Person.first.id..Person.last.id
people.each do |person_id|
  weeks.each do |week|
    5.times do |l|
      Shift.create(
        date_of_shift: (Date.today.beginning_of_week + l).weeks_since(week),
        person_id: person_id,
        shift_type_id: ShiftType.first.id + rand(10)
      )
    end
  end
end
