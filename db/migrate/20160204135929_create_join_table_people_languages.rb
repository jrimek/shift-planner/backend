class CreateJoinTablePeopleLanguages < ActiveRecord::Migration[5.0]
  def change
    create_join_table :people, :languages do |t|
      t.references :people, foreign_key: true, index: false
      t.references :languages, foreign_key: true, index: false
    end
  end
end
