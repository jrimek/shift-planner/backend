class CreateShifts < ActiveRecord::Migration[5.0]
  def change
    create_table :shifts do |t|
      t.date :date_of_shift, index: true
      t.references :person, foreign_key: true
      t.references :shift_type, foreign_key: true, index: false

      t.timestamps
    end
  end
end
