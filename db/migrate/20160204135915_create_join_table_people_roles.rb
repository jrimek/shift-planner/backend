class CreateJoinTablePeopleRoles < ActiveRecord::Migration[5.0]
  def change
    create_join_table :people, :roles do |t|
      t.references :people, foreign_key: true, index: false
      t.references :roles, foreign_key: true, index: false
    end
  end
end
