class CreateJoinTablePeopleSkills < ActiveRecord::Migration[5.0]
  def change
    create_join_table :people, :skills do |t|
      t.references :people, foreign_key: true, index: false
      t.references :skills, foreign_key: true, index: false
    end
  end
end
