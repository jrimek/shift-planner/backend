Rails.application.routes.draw do


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'
  scope "/api" do

    resources :departments, only: :index
    get 'departments/:id/week/:week' => 'departments#show'

    #resources :people, only: [:show, :create, :update, :delete]
    resources :shifts, only: :update

    resources :shift_types, only: :index
    #resources :roles, only: :index
    
    get 'people_filter_criteria/:dept_id/week/:week' => 'filters#people'

    #get 'person_shifts/:dept_id/week/:week' => 'person_shifts#index'         #unused and doesnt seem to work
    #get 'department_shifts/:dept_id/week/:week' => 'department_shifts#index' #unused
  end 
end
